/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : MyService2.js
 * @Created_at  : 11/02/2021
 * @Update_at   : 11/02/2021
 * ----------------------------------------------------------------
 */

export default class MyService2 {
    // ------------------------
    // Core methods

    /**
     * Check if value is null.
     *
     * @param value
     *
     * @return Boolean
     */
    static isNull(value) {
        // Process
        return value === null;
    }
}
