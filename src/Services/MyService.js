/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : MyService.js
 * @Created_at  : 11/02/2021
 * @Update_at   : 11/02/2021
 * ----------------------------------------------------------------
 */

// Require needed components
import MyService2 from "./MyService2";

export default class MyService {
    // ------------------------
    // Core methods

    /**
     * Test.
     *
     * @param msg
     *
     * @return *
     */
    static test(msg = null) {
        // Check
        if (MyService2.isNull(msg)) throw Error('Your message is null.');

        // Process
        return msg;
    }
}
