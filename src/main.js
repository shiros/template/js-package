/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : main.js
 * @Created_at  : 11/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed elements - Services
import MyService from "./Services/MyService";
import MyService2 from "./Services/MyService2";

// --------------------------------
// Export elements

export {
    // Services
    MyService,
    MyService2
}